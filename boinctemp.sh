#!/bin/bash
#Script to adapt BOINC's global CPU usage to CPU temperature
#Use your favorite internet search enginge
#for your CPU's suggested temperature
#Written for AMD Athlon 64 X2 (R) [2006]
#
#Released into the Public Domain or CC0, your choice.
#Thanks to grymoire.com, tldp.org, stackoverflow.com
#and all of the Free, Libre, Open Source Community.
#Version 0.6
#
#Usage:
#You can give the CPU usage as an argument 
# and the script will set it and exit
#Run it without argument and it will adjust according to temperature

#Set target temperature of your CPU in Degree Celsius here:
TARGTEMP=62

#Value changes of CPU usage in percent
USAGELEAPS=1
#Can be adjusted in line 60 and 61.

#Set interval between usage changes (in seconds)
CHANGEPAUSE=60

#Set interval between screen outputs (in minutes)
OUTPUTPAUSE=1

#Set starting, maximum and minimum percentage
MINCPU=10
MAXCPU=90
STARTCPU=7

#Set threshold for changes
THRESHUP=1

#Simple check for dependencies
DEPENDENCIES="sensors sed"
for i in $DEPENDENCIES; do
if ! command -v "$i" >/dev/null
then echo "Please check if you have the package necessary for \033[1m$i\033[0m installed." && exit 1; fi; done

#Function for displaying (a kind of) progress bar graph of advised CPU usage
output () {
	echo -n "$ADVCPU % "
	echo -n $(date +"%H:%M")
	for i in $(seq 1 "$CURRTEMP4"); do echo -n "°"; done
	echo -n " $CURRTEMP1"/"$CURRTEMP2"/"$CURRTEMP3"/"$CURRTEMP4"°C && echo
	echo -n "$CURRTEMP3°C "
	echo -n $(date +"%H:%M")
	for i in $(seq 1 "$ADVCPU"); do echo -n "%"; done
	echo -n " $ADVCPU" % && echo
}

#Change to BOINC_DIR and create temporary folder for backup
BOINCDAT=$(grep BOINC_DIR /etc/default/boinc-client | grep -v "^#" | cut -d"\"" -f2)
cd $BOINCDAT
mkdir -p tmp

#Set counter before "while loop" (v)
COUNTER=0

#Begin loop if BOINC is running
while [ -f "lockfile" ]; do
#Grab current CPU usage settings
	CURRCPU=$(grep "<cpu_usage_limit>" global_prefs_override.xml | cut -d">" -f2 | cut -d"." -f1)
#Grab current temperature
	CURRTEMP1=$(sensors | grep "Core1 Temp" | head -1 | cut -d"+" -f2 | cut -d"." -f1)
	sleep 1
	CURRTEMP2=$(sensors | grep "Core1 Temp" | head -1 | cut -d"+" -f2 | cut -d"." -f1)
	sleep 1
	CURRTEMP3=$(sensors | grep "Core1 Temp" | head -1 | cut -d"+" -f2 | cut -d"." -f1)
	sleep 1
	CURRTEMP4=$(sensors | grep "Core1 Temp" | head -1 | cut -d"+" -f2 | cut -d"." -f1)
#Make sure that advised CPU usage will not fall under 1
#	NONZEROCPU=$(( $ADVCPU
#In-/Decrease usage by ^USAGELEAPS
#	if [[ "$TARGTEMP" -gt "$CURRTEMP" && "$CURRCPU" -lt 100 && "$CURRCPU" -gt 1 ]]
#	if [[ "$TARGTEMP" -gt "$CURRTEMP" && "$ADVCPU" -lt 100-"$USAGELEAPS" && "$ADVCPU" -gt 1+"$USAGELEAPS" ]]
	if [[ "$TARGTEMP" -lt "$CURRTEMP1" && "$TARGTEMP" -lt "$CURRTEMP2" && "$TARGTEMP" -lt "$CURRTEMP3" && "$TARGTEMP" -lt "$CURRTEMP3" ]]
		then 
			if [[ "$ADVCPU" -gt 1+"$USAGELEAPS" &&  "$ADVCPU" -gt $MINCPU ]]
			then ADVCPU=$(( $CURRCPU-$USAGELEAPS ))
			else ADVCPU=$MINCPU
			fi
		fi	
	if [[ "$TARGTEMP" -ge "$CURRTEMP1" || "$TARGTEMP" -ge "$CURRTEMP2" || "$TARGTEMP" -ge "$CURRTEMP3" || "$TARGTEMP" -ge "$CURRTEMP4" ]]
		then
			if [[ "$ADVCPU" -lt 100-"$USAGELEAPS" && "$ADVCPU" -lt $MAXCPU ]]
			then ADVCPU=$(( $CURRCPU+$USAGELEAPS ))
			else ADVCPU=$MAXCPU
			fi
		fi

#echo "$ADVCPU"

#set CPU usage to 25 at start
[ "$COUNTER" -eq 0 ] && ADVCPU=$STARTCPU
#If argument given set it as CPU usage
[ -n "${1}" ] && ADVCPU=$1

#Sometimes puts advised CPU usage above 100%
#meaning that it will take longer to come back down 
#to normal percentage. If the script sets it to 0 
#it will also go to 100. So here's a warning:
	CHECKCPU=$(grep "<cpu_usage_limit>" global_prefs_override.xml | cut -d">" -f2 | cut -d"." -f1)
#	if [[ "$CHECKCPU" -gt 95 || "$CHECKCPU" -lt 5 ]]
	if [[ "$CHECKCPU" -gt 99-"$USAGELEAPS" || "$CHECKCPU" -lt 2+"$USAGELEAPS" ]]
#		then zenity --info --timeout 30 --text "Check BOINC's CPU usage limit!"
		then echo "Check BOINC's CPU usage limit!"
#
#	then
#
	fi

#Puts out advised CPU usage once at start (^ COUNTER=0)
if [ "$COUNTER" = 0 ]; then
	output
	COUNTER=1
fi

#Calculates pause for output on screen and to log by remainder
MINUTE=$(date +"%-M")
REMAINDER=$(( $MINUTE % $OUTPUTPAUSE ))
if [ "$REMAINDER" = 0 ] && [ "$MINUTE" != "$OLDMINUTE" ]; then
	output
#Save time to compare as to not output more than once in the same minute	
OLDMINUTE="$MINUTE"
fi

#Stream EDit line with CPU usage by substituting CURRCPU with ADVCPU
sed '/cpu_usage_limit/ s/'$CURRCPU'/'$ADVCPU'/' <global_prefs_override.xml >tmp/global_prefs_override.xml
#Check temporary backup and copy to original (errors go to output AND into boinctemp.log)
if grep "<cpu_usage_limit>"$ADVCPU"" tmp/global_prefs_override.xml 1>/dev/null #2&1>/dev/null
	then cp tmp/global_prefs_override.xml global_prefs_override.xml
	else echo "Can't edit preferences file. Check your disk." && exit 1
fi | tee -a boinctemp.log
boinccmd --read_global_prefs_override
#fi
#echo $?
#if usage is given as argument, exit
[ -n "${1}" ] && exit
sleep "$CHANGEPAUSE"s
done