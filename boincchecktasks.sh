#!/bin/bash
while true; do
	boinccmd --get_simple_gui_info > ~/boincsimpleguiinfo.log
	#TODAY=$(date +"%a %b %d %T %G")
	TODAY=$(date +%s)
	
	DEADLINE1=$(date --date="$(grep -A15 "1)" ~/boincsimpleguiinfo.log | grep deadline | cut -d":" -f2,3,4)" +%s)
	if [[ "$DEADLINE1" -lt " $TODAY" ]]; then
	ESTIMATED1=$(grep -A15 "1)" ~/boincsimpleguiinfo.log | grep estimated | cut -d":" -f2 | cut -d"." -f1)
	CHECKPOINTED1=$(grep -A15 "1)" ~/boincsimpleguiinfo.log | grep checkpoint | cut -d":" -f2 | cut -d"." -f1)
	if [[ "$CHECKPOINTED1" -gt "$ESTIMATED1" ]]
		then zenity --info --text  "Check BOINC tasks for time."
	fi
	fi
	
	DEADLINE2=$(date --date="$(grep -A15 "2)" ~/boincsimpleguiinfo.log | grep deadline | cut -d":" -f2,3,4)" +%s)
	if [[ "$DEADLINE2" -lt " $TODAY" ]]; then
	ESTIMATED2=$(grep -A15 "2)" ~/boincsimpleguiinfo.log | grep estimated | cut -d":" -f2 | cut -d"." -f1)
	CHECKPOINTED2=$(grep -A15 "2)" ~/boincsimpleguiinfo.log | grep checkpoint | cut -d":" -f2 | cut -d"." -f1)
	if [[ CHECKPOINTED2 -gt ESTIMATED2 ]]
		then zenity --info --text  "Check BOINC tasks for time."
	fi
	fi
	
	DEADLINE3=$(date --date="$(grep -A15 "3)" ~/boincsimpleguiinfo.log | grep deadline | cut -d":" -f2,3,4)" +%s)
	if [[ "$DEADLINE3" -lt " $TODAY" ]]; then
	ESTIMATED3=$(grep -A15 "3)" ~/boincsimpleguiinfo.log | grep estimated | cut -d":" -f2 | cut -d"." -f1)
	CHECKPOINTED3=$(grep -A15 "3)" ~/boincsimpleguiinfo.log | grep checkpoint | cut -d":" -f2 | cut -d"." -f1)
	if [[ CHECKPOINTED3 -gt ESTIMATED3 ]]
		then zenity --info --text  "Check BOINC tasks for time."
	fi
	fi
	
	sleep 1h
done